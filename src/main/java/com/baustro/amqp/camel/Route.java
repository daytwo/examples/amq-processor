package com.baustro.amqp.camel;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.camel.builder.RouteBuilder;

import com.baustro.amqp.processor.QuoteProcessor;

@ApplicationScoped
public class Route extends RouteBuilder {

    @Inject
    private QuoteProcessor processor;

    @Override
    public void configure() throws Exception {
        //@formatter:off
        from("amqp:queue:test-queue").
        routeId("FromAmq2Java").
        process(processor);
        //@formatter:on
    }
}
