package com.baustro.amqp.processor;

import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

@ApplicationScoped
public class QuoteProcessor implements Processor {

    public void process(Exchange exchange)
            throws Exception {
        String message = exchange.getMessage().getBody(String.class);
        System.out.println("Processing message: " + message);
    }
}
